const mapboxgl = require('mapbox-gl/dist/mapbox-gl.js');

export class Thumbstack {
    title;
    description;
    startDate;
    endDate;
    lat;
    lng;

    popup; // déclarer la popup


    // Faire le fichier json pour stocker l'information dans localstorage
    constructor( jsonThumbstack ) { // dès qu'on fait new Objet => le constructeur crée l'objet
        this.title = jsonThumbstack.title;
        this.description = jsonThumbstack.description;
        this.startDate = jsonThumbstack.startDate;
        this.endDate = jsonThumbstack.endDate;
        this.lat = jsonThumbstack.lat;
        this.lng = jsonThumbstack.lng;
        this.createPopup(); //appelle la méthode qui crée la popup
        
    }

    createPopup() { // créer la popup
        this.popup = new mapboxgl.Popup(); 
        
        /* Formatage de la date de départ */
        const start_date = new Date(this.startDate);
            const start_monthDay = start_date.getDate();
                const start_month_toString = {month: 'long'};
                const string_start_month = new Intl.DateTimeFormat('fr-FR', start_month_toString).format(this.startDate);
        
            const start_year = start_date.getFullYear();
                const start_weekday_toString = {weekday: 'long'};
                const string_start_weekDay = new Intl.DateTimeFormat('fr-FR', start_weekday_toString).format(this.startDate);
        
        /* Formatage de la date de fin */
        const end_date = new Date(this.endDate);
        const end_monthDay = end_date.getDate();
            const end_month_toString = {month: 'long'};
            const string_end_month = new Intl.DateTimeFormat('fr-FR', end_month_toString).format(this.endDate);
    
        const end_year = end_date.getFullYear();
            const end_weekday_toString = {weekday: 'long'};
            const string_end_weekDay = new Intl.DateTimeFormat('fr-FR', end_weekday_toString).format(this.endDate);

        // Format souhaité : Du samedi 13 mai 2021 au mercredi 18 mai 2021
        this.popup.setHTML(`
            <h4>${this.title}</h4>
            <h4>${this.description}</h4>
            <h4>Du ${string_start_weekDay} ${start_monthDay} ${string_start_month} ${start_year} au ${string_end_weekDay} ${end_monthDay} ${string_end_month} ${end_year}</h4>
            `);
    };

    
    getMarker() { // créer le marker

        /* Calcul du temps restant avant le spectacle */
        const start__date = new Date(this.startDate);
        const timebeforeshow = start__date.getTime() - Date.now();
            const toNumberOfDays = timebeforeshow / (1000 * 60 * 60 * 24);

        const timeBeforeShow = toNumberOfDays;
        let marker;
        if (timeBeforeShow > 3) {
            marker = new mapboxgl.Marker({color: '#09eb10' });
        } else if (timeBeforeShow < 0) {
            marker = new mapboxgl.Marker({color: '#f00' });
        } else {
            marker = new mapboxgl.Marker({color: '#ff7f00' });
        }


        /* Formatage de la date de départ */
        const start_date = new Date(this.startDate);
            const start_monthDay = start_date.getDate();
                const start_month_toString = {month: 'long'};
                const string_start_month = new Intl.DateTimeFormat('fr-FR', start_month_toString).format(this.startDate);
        
            const start_year = start_date.getFullYear();
                const start_weekday_toString = {weekday: 'long'};
                const string_start_weekDay = new Intl.DateTimeFormat('fr-FR', start_weekday_toString).format(this.startDate);
        
        /* Formatage de la date de fin */
        const end_date = new Date(this.endDate);
        const end_monthDay = end_date.getDate();
            const end_month_toString = {month: 'long'};
            const string_end_month = new Intl.DateTimeFormat('fr-FR', end_month_toString).format(this.endDate);
    
        const end_year = end_date.getFullYear();
            const end_weekday_toString = {weekday: 'long'};
            const string_end_weekDay = new Intl.DateTimeFormat('fr-FR', end_weekday_toString).format(this.endDate);

        marker.setLngLat({lng:this.lng, lat:this.lat}); // entre parenthèse, c'est un JSON - Attribuer Latitude et Longitude au marker
        
        // Au survol de la souris sur un Marker on voit le titre et les dates de l’événement
        marker.getElement().title = this.title + ' : Du ' + string_start_weekDay + ' ' + start_monthDay + ' ' + string_start_month + ' ' + start_year + ' au ' + string_end_weekDay + ' ' + end_monthDay + ' ' + string_end_month + ' ' + end_year ; // retourne l'élément HTML du marker : permet de faire apparaitre l'élément lors du survol du marker
        // getElement récupère le div

        //Rattachement de la popup au marker
        marker.setPopup( this.popup ); // add popup

        return marker; // return pour pouvoir se servir de marker ailleurs (dans App.js)
    };

    

        // Appelée automatiquement par JSON.stringify()
        toJSON() {
            return {
                title: this.title,
                description: this.description,
                startDate: this.startDate,
                endDate: this.endDate,
                lat: this.lat,
                lng: this.lng
            }
        }
}   