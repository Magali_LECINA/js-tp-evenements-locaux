import '../index.html'; // charger l'HTML
import '../style.css'; // charger le fichier style CSS
import '../reset.css'; // charger le fichier reset CSS
import appConfig from '../../app.config';

import { Thumbstack } from './Thumbstack'; // importer la Classe Thumbstack

import 'mapbox-gl/dist/mapbox-gl.css'; // importer la CSS de Mapbox

const mapboxgl = require('mapbox-gl/dist/mapbox-gl.js'); // Mapbox = import syntaxe node js

class App { // Créer Classe App
    domNewMap; // Créer les éléments HMTL utiles

    domNewTitle;
    domNewDescription;
    domNewStartDate;
    domNewEndDate;
    domNewLat;
    domNewLng;
    domBtnNewAdd;
    domBtnClearAll;

    map;

    thumbstacks = []; // Créer le tableau des punaises

    start() { // Initialiser l'application ?
        console.log( 'Application started' );
        this.initHtml(); // relier js au formulaire ?
        this.initMap(); // afficher la carte ?
        //this.loadThumstacks(); // stocker données des punaises ?
        //this.renderThumbstacks(); // afficher les punaises ?
    }

    initHtml() {
        this.domNewTitle = document.querySelector( '#new-title' );
        // this.domNewTitle.addEventListener( 'focus', this.onErrorRemove.bind(this));

        this.domNewDescription = document.querySelector( '#description' );
        // this.domNewDescription.addEventListener( 'focus', this.onErrorRemove.bind(this));

        this.domNewStartDate = document.querySelector( '#start' );
        // this.domNewStartDate.addEventListener( 'focus', this.onErrorRemove.bind(this));

        this.domNewEndDate = document.querySelector( '#end' );
        // this.domNewEndDate.addEventListener( 'focus', this.onErrorRemove.bind(this));

        this.domNewLat = document.querySelector( '#lat' );
        // this.domNewLat.addEventListener( 'focus', this.onErrorRemove.bind(this));

        this.domNewLng = document.querySelector( '#lng' );
        // this.domNewLng.addEventListener( 'focus', this.onErrorRemove.bind(this));

        this.domBtnNewAdd = document.querySelector( '#new-add' );
        this.domBtnNewAdd.addEventListener( 'click', this.onBtnNewAddClick.bind(this) );

        this.domBtnClearAll = document.querySelector( '#clear-all' );
        this.domBtnClearAll.addEventListener( 'click', this.onBtnClearAllClick.bind(this) );
    }

    initMap() { // ligne 69 Key
        mapboxgl.accessToken = 'pk.eyJ1IjoibWFnYWxpbGVjaW5hIiwiYSI6ImNrbmZvaWJvMTJ4OG8ycHA5NzBjZWd6cTgifQ.5vZCce4rg41WU7MvrXGabg';
        this.map = new mapboxgl.Map({
        container: 'map',
        center: { lng: 2.7885292, lat: 42.6832109 }, // centrage : coordonnées
        zoom: 14,
        customAttribution: 'Rappels d\'évènements locaux', // nom ?
        style: 'mapbox://styles/mapbox/streets-v11'
        });

        // Ajout d'un bouton controle du zoom
        const zoomCtrl = new mapboxgl.NavigationControl(); // cf documentation Mapbox ?
        this.map.addControl( zoomCtrl );

        // Ajout d'un écouteur de clic sur la map
        this.map.on( 'click', (evt) => { // fonction fléchée pour ne pas modifier le this
            // click remplit champs lat lng
            // console.log(evt);
            // 1- récupérer les coordonnées avec evt
            // 2- remplit champs lat lng (domNewLat => .value )
            this.domNewLat.value = evt.lngLat.lat;
            this.domNewLng.value = evt.lngLat.lng;

        });

    }

    onBtnNewAddClick() {
        // TODO: vérifier la saisie (que value correspondent à ce qu'on attend exemple : pas de nombre dans le Titre, pas de vide...)
        // Crée l'évènement qui sert au Thumbstack
        const startDate = new Date(this.domNewStartDate.value);
        const endDate = new Date(this.domNewEndDate.value);
        const ts = new Thumbstack({
            title: this.domNewTitle.value,
            description: this.domNewDescription.value,
            startDate: startDate.getTime(),
            endDate: endDate.getTime(),
            lat: this.domNewLat.value,
            lng: this.domNewLng.value
        });

        const tsmarker = ts.getMarker();
        tsmarker.addTo( this.map );
    }

        // console.log(tsmarker);
        // déterminer la couleur du marker selon la date
        // déclarer une variable qui va calculer si la date est à plus de 3 jours, 
        // autant ou moins de 3 jours, 
        // date passée
        // cf timestamp plus bas // const now = Date.now();

        // let dateToday = new Date();
        /* Version longue
        let isoDate = dateToday.toISOString(); // "2021-03-29T14:19:55.097Z"
        let arrDate = isoDate.split('T'); // [ "2021-03-29", "14:19:55.097Z" ]
        let dateValue = arrDate[0]; // "2021-03-29"
        domInputDateFrom.value = dateValue;
        */
        // domInputDateFrom.value = dateToday.toISOString().split('T')[0];

        


                // TODO: new Thumbstack
            /*const pinaise = new mapboxgl.Marker({ 
                switch (startDate) {
                    case (//TODO:startDate - now > 3): 
                        color: '#green' 
                        break;

                    case (//TODO:startDate - now =< 3): 
                        color: '#orange' 
                        break;
                    
                    case value:
                        (//TODO:startDate - now < 0): 
                        color: '#red' 
                        break;
                
                    default:
                        break;
                }
                */


    

    loadThumstacks() {
        const storageContent = localStorage.getItem( appConfig.localStorageName );
        if( storageContent === null ) {
            return;
        }

        let storedJson;

        try {
            storedJson = JSON.parse( storageContent );
        }

        catch( error ) {
            localStorage.removeItem( appConfig.localStorageName );
            return;
        }

        for( let jsonThumbstack of storedJson ) {
            const thumbstack = new Thumbstack( jsonThumbstack );
            this.thumbstacks.push( thumbstack );
        }
    }

    // renderThumbstacks() {
    //     // TODO: domList ?
    //     this.domList.innerHTML = '';

    //     for( let thumbstack of this.thumbstacks ) {
    //         this.domList.appendChild( thumbstack.domContainer );
    //     }

        
    // }

    // deletePostIt( postIt ) {
    //     const newList = this.postIts.filter( ( p ) => { return p !== postIt } );
    //     this.postIts = newList;
    // }

/* ---  Gestionnaire d'évènements  --- */
//     onErrorRemove(evt) {
//         evt.target.classList.remove( 'error' );
//     }

//     onBtnNewAddClick() {
//         let hasError = false;
//         const newTitle = this.domNewTitle.value.trim()
//         if( newTitle === '' ) {
//             this.domNewTitle.classList.add( 'error' );
//             this.domNewTitle.value = '';
//             hasError = true;
//         }

//         if ( hasError ) {
//             return;
//         }

//         const newDescription = this.domNewDescription.value.trim();
//         if( newDescription === '' ) {
//             this.newDescription.classList.add( 'error' );
//             this.newDescription.value = '';
//             hasError = true;
//         }
        
//         // 2- Création du timestamp
//         const now = Date.now();

//         // 3- Création d'une version JSON du PostIt, puis vidange des champs
//         const newJson = {
//             title: this.domNewTitle.value,
//             description: this.domNewDescription.value,
//             startDate: now, // TODO: Adapter les 4 lignes sqq
//             endDate: now,
//             lat: '',
//             lng: ''
//         };
//         this.domNewTitle.value = this.domNewDescription.value = '';

//         // 4- Fabriquer le PostIt
//         const newThumbstack = new Thumbstack( newJson );

//         // 5- Ajout sur la page
//         this.domList.appendChild( newThumbstack.domContainer );
        
//         // 6- Enregistrement à plusieurs niveaux
//         this.thumbstacks.push( newThumbstack );
//         // console.log( this.PostIts );

//         // 7- Enregistrement dans localStorage
//         localStorage.setItem( appConfig.localStorageName, JSON.stringify( this.thumbstacks ));
//     }

       onBtnClearAllClick() {
        this.domNewTitle.value = '';

        this.domNewDescription.value = '';

        this.domNewStartDate.value = '';

        this.domNewEndDate.value = '';

        this.domNewLat.value = '';

        this.domNewLng.value = '';

       }


}

const instance = new App(); // Pour qu'on ait une seule session

export default instance;